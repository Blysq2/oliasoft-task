import React from 'react';
import PropTypes from 'prop-types';
import './Box.scss';

const Box = ({ title, children }) => (
  <div className='box'>
    { title &&
      <div className='box__header'>{title}</div>
    }
    <div className='box__content'>
      {children}
    </div>
  </div>
);

Box.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node.isRequired,
}

export default Box;
