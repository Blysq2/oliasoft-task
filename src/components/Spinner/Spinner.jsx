import React from 'react';
import './Spinner.scss';

const Spinner = () => (
  <div className='spinner'>
    <div className='spinner__circles' />
  </div>
);

export default Spinner;
