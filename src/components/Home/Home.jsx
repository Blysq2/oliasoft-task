import React from 'react';
import PropTypes from 'prop-types';
import StoryList from '../StoryList/StoryList';
import Box from '../Box/Box';

const Home = props => (
  <Box title='Stories'>
    <StoryList stories={props.stories} />
  </Box>
);

Home.propTypes = {
  stories: PropTypes.array.isRequired,
};

export default Home;
