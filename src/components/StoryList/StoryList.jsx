import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Moment from 'moment';
import './StoryList.scss';

class StoryList extends PureComponent {
  constructor(props) {
    super(props);

    this.handleClick = ::this.handleClick;
  }

  handleClick(storyId) {
    this.context.router.history.push(`/details/${storyId}`);
  }

  render() {
    const { stories } = this.props;

    return (
      <table className='story-list'>
        <thead>
          <tr>
            <th>Title</th>
            <th>Score</th>
            <th>Author</th>
            <th>Type</th>
            <th>Created</th>
          </tr>
        </thead>
          <tbody>
            {stories.map((story) => (
              <tr key={story.id} onClick={() => this.handleClick(story.id)}>
                <td>{story.title}</td>
                <td>{story.score}</td>
                <td>{story.by}</td>
                <td>{story.type}</td>
                <td>{Moment(story.time).format('DD MMM YYYY')}</td>
              </tr>
            ))}
          </tbody>
      </table>
    )
  }
}

StoryList.contextTypes = {
  router: PropTypes.object.isRequired,
};

StoryList.propTypes = {
  stories: PropTypes.array.isRequired,
};

export default StoryList;
