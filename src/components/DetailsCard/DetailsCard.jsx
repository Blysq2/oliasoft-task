import React from 'react';
import PropTypes from 'prop-types';
import Moment from 'moment';
import { Parser } from 'html-to-react';
import './DetailsCard.scss';

const DetailsCard = ({ story }) => {
  const htmlToReactParser = new Parser();
  return (
    <div className='details-card'>
      <div className='details-card__detail'>
        Title: <span>{story.title}</span>
      </div>
      <div className='details-card__detail'>
        Score: <span>{story.score}</span>
      </div>
      <div className='details-card__detail'>
        Author: <span>{story.by}</span>
      </div>
      <div className='details-card__detail'>
        Type: <span>{story.type}</span>
      </div>
      <div className='details-card__detail'>
        Created: <span>{Moment(story.time).format('DD MMM YYYY')}</span>
      </div>
      <p className='details-card__comments-title'>Comments</p>
      <div className='details-card__comments'>
      { story.comments.map(comment => (
          <div key={comment.id} className='details-card__comment'>
            <p className='details-card__by'>{comment.by}</p>
            {htmlToReactParser.parse(comment.text)}
          </div>
        ))
      }
      </div>
    </div>
  );
};

DetailsCard.propTypes = {
  story: PropTypes.object.isRequired,
};

export default DetailsCard;
