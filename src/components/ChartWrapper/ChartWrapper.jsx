import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  FlexibleWidthXYPlot,
  HorizontalGridLines,
  VerticalGridLines,
  LineMarkSeries,
  XAxis,
  YAxis,
  Crosshair,
} from 'react-vis';
import './ChartWrapper.scss';

class ChartWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      crosshairValues: []
    };

    this._onMouseLeave = this._onMouseLeave.bind(this);
    this._onNearestX = this._onNearestX.bind(this);
  }

  _onNearestX(value, {index}) {
    this.setState({crosshairValues: [this.props.data[index]]});
  }

  _onMouseLeave() {
    this.setState({crosshairValues: []});
  }

  render() {
    const { data } = this.props;

    return (
      <FlexibleWidthXYPlot onMouseLeave={this._onMouseLeave} height={200} color='#00a8ff' stroke='#00a8ff'>
        <XAxis />
        <YAxis />
        <HorizontalGridLines />
        <VerticalGridLines />
        <LineMarkSeries onNearestX={this._onNearestX} data={data} />
        <Crosshair values={this.state.crosshairValues} className={'test-class-name'} itemsFormat={(d) => [{title: 'Score', value: data[d[0].x].y}]}/>
      </FlexibleWidthXYPlot>
    );
  }
}

ChartWrapper.propTypes = {
  data: PropTypes.array.isRequired,
};

export default ChartWrapper;
