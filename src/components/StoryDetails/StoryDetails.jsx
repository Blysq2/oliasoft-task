import React from 'react';
import PropTypes from 'prop-types';
import Box from '../Box/Box';
import DetailsCard from '../DetailsCard/DetailsCard';

const StoryDetails = ({ story }) => (
  <Box title='Story details'>
    <DetailsCard story={story} />
  </Box>
);

StoryDetails.propTypes = {
  story: PropTypes.object.isRequired,
};

export default StoryDetails;
