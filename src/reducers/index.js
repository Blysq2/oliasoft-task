import { combineReducers } from 'redux';
import storiesReducer from './storiesReducer';

const rootReducer = combineReducers({
  storiesDetails: storiesReducer,
});

export default rootReducer;
